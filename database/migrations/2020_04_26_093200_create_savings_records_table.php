<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSavingsRecordsTable extends Migration {

	public function up()
	{
		Schema::create('savings_records', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('savings_id');
			$table->integer('amount');
			$table->datetime('date');
		});
	}

	public function down()
	{
		Schema::drop('savings_records');
	}
}