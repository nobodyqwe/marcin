<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSavingsTable extends Migration {

	public function up()
	{
		Schema::create('savings', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name');
			$table->integer('target_amount');
			$table->datetime('start_date');
			$table->datetime('end_date');
		});
	}

	public function down()
	{
		Schema::drop('savings');
	}
}