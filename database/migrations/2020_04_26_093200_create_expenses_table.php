<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExpensesTable extends Migration {

	public function up()
	{
		Schema::create('expenses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('category_id');
			$table->integer('amount');
			$table->text('comment');
			$table->datetime('date');
		});
	}

	public function down()
	{
		Schema::drop('expenses');
	}
}