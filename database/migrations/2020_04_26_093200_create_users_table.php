<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('last_name');
			$table->string('email');
			$table->string('password');
			$table->integer('balance');
			$table->datetime('created_at');
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}