<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavingsRecords extends Model 
{

    protected $table = 'savings_records';
    public $timestamps = false;

}