<?php 

namespace App\Http\Controllers;

use App\Savings;
use App\Users;
use Illuminate\Http\Request;

class SavingsController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
        $data = $request->all();

      if (str_contains($data['amount'],'.')){
          $data['amount'] = str_replace(".",'',$data['amount']);
      }
      Savings::create($data);
      return response();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      $user = Users::find($id);
      $total = 0;
      $target = $user->target_amount;

      foreach ($user->Savings as $item) {
          $total += $item->amount;
      }

      $target_reach = round(($total*100)/($target));
      return response()->json(['total'=>$total,'reach'=>$target_reach,'target'=>$target]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>