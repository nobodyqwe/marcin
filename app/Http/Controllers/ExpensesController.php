<?php 

namespace App\Http\Controllers;

use App\Other30;
use App\Other50;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExpensesController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  public function store(Request $request)
  {
      $model = null;
      if ($request->get('exType','') == '50') {
          $model = new Other50();
      }else{
          $model = new Other30();
      }
      $model->user_id = $request->get('user_id');
      $model->amount = $request->get('amount',0);
      if (str_contains($model->amount,'.')){
          $model->amount = str_replace(".",'',$model->amount);
      }else{
          $model->amount = $model->amount*100;
      }
      $model->comment = $request->get('comment','');
      $model->date = Carbon::now();
      $model->type = $request->get('type');
      $model->saveOrFail();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>