<?php 

namespace App\Http\Controllers;

use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UsersController extends Controller 
{

    public function checkLogin(Request $request)
    {
        $user = Users::where('email',$request->get('email'))->where('password',$request->get('pass'))->first();
        if ($user != null){
            return response('OK '.$user->id);
        }

        return response('FAIL');
    }

    public function register(Request $request)
    {
        if (strlen($request->get('email')) < 6 && strlen($request->get('pass')) < 4){
            return response('FAIL');
        }
        $user = new Users();
        $user->email = $request->get('email');
        $user->password = $request->get('pass');
        $user->save();
        return response('OK');
    }

    public function getTotal(Request $request, $id)
    {

        $user = Users::find($id);
        $total_50 = 0;
        $total_30 = 0;
        $expenses = $user->Other50()->where('date','>',Carbon::now()->startOfMonth())->get();
        foreach ($expenses as $item){
            $total_50 += $item->amount;
        }
        $user->total_50 = $total_50;
        $user->perc_50 = round(($total_50*100)/($user->balance/2));
        $expenses = $user->Other30()->where('date','>',Carbon::now()->startOfMonth())->get();
        foreach ($expenses as $item){
            $total_30 += $item->amount;
        }
        $user->total_30 = $total_30;
        $user->perc_30 = round(($total_30*100)/($user->balance*0.3));

        $total = $total_50+$total_30;

        return response()->json(['total'=>$total]);
    }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      Users::create($request->all());
      return response();
  }

    public function goal(Request $request)
    {
        $user = Users::find($request->get('user_id'));
        $user->target_amount = $request->get('amount');
        $user->save();
        return response();
    }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      $user = Users::find($id);
      $total_50 = 0;
      $total_30 = 0;
      $expenses = $user->Other50()->where('date','>',Carbon::now()->startOfMonth())->get();
      foreach ($expenses as $item){
          $total_50 += $item->amount;
      }
      $user->total_50 = $total_50;
      $user->perc_50 = round(($total_50*100)/($user->balance/2));
      $expenses = $user->Other30()->where('date','>',Carbon::now()->startOfMonth())->get();
      foreach ($expenses as $item){
          $total_30 += $item->amount;
      }
      $user->total_30 = $total_30;
      $user->perc_30 = round(($total_30*100)/($user->balance*0.3));
      $user->balance = $user->balance-($total_30+$total_50);

      return response()->json($user);
  }

    public function balance(Request $request)
    {
        $user = Users::find($request->get('user_id'));
        $amount = $request->get('amount',0);

        if (str_contains($amount,'.')){
            $amount = str_replace(".",'',$amount);
        }else{
            $amount = $amount*100;
        }
        $user->balance += $amount;
        $user->saveOrFail();
        return response();
    }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request,$id)
  {
    Users::find($id)->update($request->all());
    return response();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    Users::destroy($id);
    return response();
  }
  
}

?>