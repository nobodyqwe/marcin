<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('users', 'UsersController',['only'=>['show', 'destroy', 'store', 'update']]);
Route::post('checkLogin', 'UsersController@checkLogin');
Route::post('register', 'UsersController@register');
Route::post('goal', 'UsersController@goal');
Route::post('balance', 'UsersController@balance');
Route::get('getTotal/{id}', 'UsersController@getTotal');
Route::resource('savings', 'SavingsController',['only'=>['show', 'destroy', 'store', 'update']]);
Route::resource('expenses', 'ExpensesController',['only'=>['show', 'destroy', 'store', 'update']]);
Route::resource('categories', 'CategoriesController',['only'=>['show', 'destroy', 'store', 'update']]);
