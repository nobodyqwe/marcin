<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model 
{

    protected $table = 'expenses';
    public $timestamps = false;

    public function Category()
    {
        return $this->hasOne('App\Categories', 'category_id');
    }

}