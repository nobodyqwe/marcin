<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model 
{

    protected $table = 'users';
    public $timestamps = false;

    public function Savings()
    {
        return $this->hasMany('App\Savings', 'user_id');
    }

    public function Other50()
    {
        return $this->hasMany('App\Other50', 'user_id');
    }

    public function Other30()
    {
        return $this->hasMany('App\Other30', 'user_id');
    }

}