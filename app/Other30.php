<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other30 extends Model
{
    //
    protected $table = 'other30';
    public $timestamps = false;
}
