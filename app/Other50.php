<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other50 extends Model
{
    //
    protected $table = 'other50';
    public $timestamps = false;
}
