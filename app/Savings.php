<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Savings extends Model 
{

    protected $table = 'savings20';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'comment',
        'amount','name'];

    public function Records()
    {
        return $this->hasMany('App\SavingsRecords', 'savings_id');
    }

}