{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('user_id', 'User_id:') !!}
			{!! Form::text('user_id') !!}
		</li>
		<li>
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name') !!}
		</li>
		<li>
			{!! Form::label('target_amount', 'Target_amount:') !!}
			{!! Form::text('target_amount') !!}
		</li>
		<li>
			{!! Form::label('start_date', 'Start_date:') !!}
			{!! Form::text('start_date') !!}
		</li>
		<li>
			{!! Form::label('end_date', 'End_date:') !!}
			{!! Form::text('end_date') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}